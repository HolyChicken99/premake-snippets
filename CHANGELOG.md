# Change Log

All notable changes to the "premake-snippets" extension will be documented in this file.

## [1.0.0]

- Initial release of premake-assistant

## [1.1.1]

- added more snippets
