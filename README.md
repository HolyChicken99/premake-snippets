# premake-snippets README
The premake-snippets extension for Visual Studio code is helpful for those that are using premake as their build environment for projects using C++

<img alt="Visual Studio Marketplace Rating (Stars)" src="https://img.shields.io/visual-studio-marketplace/stars/holychicken99.premake-snippets?style=for-the-badge"> 

<img alt="Visual Studio Marketplace Version" src="https://img.shields.io/visual-studio-marketplace/v/holychicken99.premake-snippets?style=for-the-badge">


## Features
 ### Code completion for C/C++ Projects
![feature](assets/snippet.gif)

Available Snippets


|Trigger      | Description |
| ----------- | ----------- |
| Pc          | Premake basic setup for C |
| Pcpp  | Premake basic setup for C++ |
| Pld       | path to libraries directory |
| Pos  | Add platform configuration |






---
## Known Issues

Does not currently support all completions

## Release Notes

Users appreciate release notes as you update your extension.

### 1.0.0

Initial release of premake-snippets

### 1.0.1 
Added more snippets

### 1.0.4 
Added more 

### About Premake
Premake is an awesome build environment . As opposed to the widely used build environment CMake, it is much simpler and more flexible which allows users to build their C/C++ Projects much faster.


**Enjoy!** this snippet pack